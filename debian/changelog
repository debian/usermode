usermode (1.114-3) unstable; urgency=medium

  * update copyright info:
    + use License-Grant and Reference;
      add lintian overrides about License-Reference field
    + update coverage
    + license packaging as GPL-3+ (not GPL-2+)
  * simplify source helper script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 18 Sep 2021 10:15:51 +0200

usermode (1.114-2) experimental; urgency=medium

  * stop generate and ship pixmap files unused since release 1.109-1;
    closes: bug#989007, thanks to Pino Toscano
  * use debhelper compatibility level 13 (not 12)

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 May 2021 12:08:30 +0200

usermode (1.114-1) experimental; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * drop patch 0002 adopted upstream
  * include example scripts with binary package
  * drop README.Debian note about changed .desktop categories,
    obsoleted by upstream change since 1.109 in 2012

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 May 2021 09:24:55 +0200

usermode (1.113-4) unstable; urgency=medium

  * stop build-depend on libglade2-dev
    (unused since upstream release 1.101);
    closes: bug#967890, thanks to Simon McVittie

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 26 Oct 2020 19:59:21 +0100

usermode (1.113-3) unstable; urgency=medium

  * adopt package; closes: bug#858661
  * add git-buildpackage config:
    + use pristine-tar
    + sign tags
    + filter out any .git* files
  * add source script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 25 Oct 2020 14:23:27 +0100

usermode (1.113-2) unstable; urgency=medium

  * QA upload.
  * debian/patches/0002: Stop using deprecated selinux headers.
    (Closes: #956353)

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.

 -- Boyuan Yang <byang@debian.org>  Fri, 10 Apr 2020 12:06:56 -0400

usermode (1.113-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * debian/control:
    + Bump debhelper compat to v12.
    + Bump Standards-Version to 4.4.1.
    + Update project homepage to use the new one on pagure.io.
  * debian/rules:
    + Enable full hardening.
    + Enable "dh_missing --fail-missing".
  * debian/copyright:
    + Use new homepage.
    + Use secure URI when possible.
  * debian/watch: Update to v4 format and monitor pagure releases.
  * debian/patches: Drop patches that are no longer necessary.
  * debian/post: Removed, triggers are automatically triggered
    and do not need human intervention.

 -- Boyuan Yang <byang@debian.org>  Wed, 09 Oct 2019 19:32:33 -0400

usermode (1.109-3) unstable; urgency=medium

  * QA upload.
  * Explicitly set paths to fdformat, et.al. instead of looking them
    up on the build system. (Closes: #914933)
  * Fix lintian command-in-menu-file-and-desktop-file by dropping
    the debian/menu file.
  * Fix lintian dep5-copyright-license-name-not-unique by making
    a single licence entry in debian/copyright.

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 05 Jan 2019 19:13:42 +0100

usermode (1.109-2) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group. (see #858661)
  * Fix the build with glibc 2.28.
  * Use default compression for the source package.

 -- Adrian Bunk <bunk@debian.org>  Sun, 09 Dec 2018 13:44:22 +0200

usermode (1.109-1) unstable; urgency=low

  * New maintainer (Closes: #583242).
  * New upstream release.
  * Bump to Standards-Version 3.9.3.
  * Set debian/compat to 9.
  * Add Vcs-{Browser,Git} pointing to collab-maint/usermode.git.
  * Add debian/patches/fix.format.security.FTBFS.patch.
  * Add debian/usermode.lintian-overrides (setuid-binary).
  * Update debian/copyright.

 -- Khalid El Fathi <khalid@elfathi.fr>  Tue, 06 Mar 2012 22:37:13 +0100

usermode (1.108-1) unstable; urgency=low

  * QA upload.
  * New upstream release.

 -- Muammar El Khatib <muammar@debian.org>  Fri, 06 Jan 2012 23:17:28 +0100

usermode (1.107-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
  * Switch to DH7:
    - Update debian/rules.
    - Add debian/usermode.install to install .xpm files.
    - Add debian/clean to remove generated .xpm files.
    - Add debhelper as a build dependency.
    - Add ${misc:Depends} as a dependency.
    - Set debian/compat to 8.
  * Add a conditional to strip or not depending on DEB_BUILD_OPTIONS value.
    (Closes: #438243)
  * fix.man.hyphen.used.as.minus.sign.patch: use hyphen instead of
    minus sign in man page.
  * debian/source/local-options: unapply patches after build.
  * Split Build-Depends value in multiple lines.
  * Bump to Standards-Version 3.9.2. No changes required.
  * Update debian/copyright to DEP-5.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Sat, 18 Jun 2011 22:20:27 +0200

usermode (1.106.1-2) unstable; urgency=low

  * QA upload.
  * Build depend on gettext
    Closes: 553978
  * Update debian/watch
    Closes: 450301

 -- Anibal Monsalve Salazar <anibal@debian.org>  Tue, 18 Jan 2011 09:56:57 +1100

usermode (1.106.1-1) unstable; urgency=low

  * QA upload.
  * New upstream release

 -- Anibal Monsalve Salazar <anibal@debian.org>  Mon, 17 Jan 2011 23:29:39 +1100

usermode (1.106-1) unstable; urgency=low

  * QA upload.
  * New upstream release
  * Fix out-of-date-standards-version

 -- Anibal Monsalve Salazar <anibal@debian.org>  Tue, 07 Sep 2010 12:24:16 +1000

usermode (1.105-0.1) unstable; urgency=low

  * Non-maintainer upload
  * New upstream release

 -- Anibal Monsalve Salazar <anibal@debian.org>  Sat, 24 Apr 2010 16:24:13 +1000

usermode (1.104-0.1) unstable; urgency=low

  * Non-maintainer upload
  * New upstream release
  * Debian source format is 3.0 (quilt)
  * Fix out-of-date-standards-version

 -- Anibal Monsalve Salazar <anibal@debian.org>  Sat, 27 Mar 2010 17:03:22 +1100

usermode (1.102-0.1) unstable; urgency=low

  * Non-maintainer upload
  * New upstream release
    Build depend on intltool, libblkid-dev
  * Update debian/watch
  * Update debian/copyright
  * Add homepage header in debian/control
  * Remove not used debian/patch
  * Fix debian-rules-ignores-make-clean-error
  * Fix ancient-standards-version
  * Fix maintainer-script-ignores-errors
  * Fix description-contains-duplicated-word
  * Fix no-upstream-changelog
  * Fix menu-item-uses-apps-section
  * Fix menu-item-creates-new-section

 -- Anibal Monsalve Salazar <anibal@debian.org>  Wed, 11 Nov 2009 15:17:11 +1100

usermode (1.81-3.2) unstable; urgency=low

  * NMU.
  * Fixed failure to validate the created desktop file.
    Patch by Kumar Appaiah <akumar@ee.iitm.ac.in>
    Closes: #461028

 -- Anibal Monsalve Salazar <anibal@debian.org>  Fri, 22 Feb 2008 23:14:29 +1100

usermode (1.81-3.1) unstable; urgency=low

  * NMU.
  * Fixed "FTBFS: error: Categories values must be one of ...".
    Patch by Ana Guerrero <ana@ekaia.org>. Closes: #378026.
  * Fixed "FTBFS: bashisms". Patch by Jonas Meyer <shitse@web.de>.
    Closes: #374911.

 -- Anibal Monsalve Salazar <anibal@debian.org>  Fri, 18 Aug 2006 16:20:19 +1000

usermode (1.81-3) unstable; urgency=low

  * replaced xlibs-dev build-dep with libice-dev and libsm-dev, closes: #346943.
  * removed unnecessary libglib2.0-dev dependency
  * added watch file

 -- Christopher Cramer <crayc@dapac.org>  Wed, 11 Jan 2006 00:15:54 -0800

usermode (1.81-2) unstable; urgency=low

  * added /usr/sbin to dpkg-shlibdeps command, closes: #336829.

 -- Christopher Cramer <crayc@dapac.org>  Tue,  1 Nov 2005 11:12:27 -0800

usermode (1.81-1) unstable; urgency=low

  * New upstream release.
  * Put myself as co-maintainer.

 -- Anibal Monsalve Salazar <anibal@debian.org>  Tue, 25 Oct 2005 18:52:10 +1000

usermode (1.80-3) unstable; urgency=low

  * Changed libuser-dev build-dependency to libuser1-dev and added
    libxml-parser-perl as a build-dependecy in debian/control.
    (from Anibal Monsalve Salazar <anibal@debian.org>)
  * Set Standards-Version to 3.6.2 in debian/control. (Ibid)
  * Ignored "$(MAKE) distclean" in debian/rules. (Ibid)

 -- Christopher Cramer <crayc@dapac.org>  Wed, 24 Aug 2005 13:55:59 -0700

usermode (1.80-2) unstable; urgency=low

  * Provide xpm icons for Debian menu
  * Provide man page for consolehelper-gtk
  * Remove pam-panel-icon; it requires functionality not present in Debian
  * Remove unused pixmaps
  * Remove superfluous /usr/bin/shutdown and its configuration files

 -- Christopher Cramer <crayc@dapac.org>  Thu, 26 May 2005 12:24:53 -0700

usermode (1.80-1) unstable; urgency=low

  * Initial package, closes: #310643.

 -- Christopher Cramer <crayc@dapac.org>  Tue, 24 May 2005 16:33:36 -0700
